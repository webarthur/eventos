# MiniDebConf Curitiba 2018

Material gráfico criado para a MiniDebConf Curitiba 2018 realizada nos dias 11 a
14 de abril na UTFPR.

Autores:

* Angelo Guimarães Rosa (angelorosa@gmail.com)
* Paulo Henrique de Lima Santana.

Arquivos SVG e PNG em vários tamanhos.

Site: https://minidebconf.curitiba.br

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/) 
