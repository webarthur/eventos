# MiniDebConf Curitiba

A MiniDebConf Curitiba é um evento aberto a todos(as), independente do seu nível de conhecimento sobre Debian. O mais importante será reunir novamente a comunidade para celebrar o maior projeto de Software Livre no mundo, por isso queremos receber desde usuários(as) inexperientes que estão iniciando o seu contato com o Debian até Desenvolvedores(as) oficiais do projeto. Ou seja, estão todos(as) convidados(as)!

MiniDebConfs são encontros locais organizados por membros do Projeto Debian para atingir objetivos semelhantes aos da DebConf, mas em um contexto regional. Durante todo o ano são organizadas MiniDebConfs ao redor do mundo, como se pode ver nesta página.
